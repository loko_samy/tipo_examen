# Generated by Django 2.1.2 on 2018-12-07 03:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pruebaapp', '0003_auto_20181204_2230'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tecnico',
            name='Raza',
        ),
        migrations.AlterField(
            model_name='tecnico',
            name='titulo',
            field=models.CharField(max_length=100, unique=True, verbose_name='Nombre'),
        ),
    ]
