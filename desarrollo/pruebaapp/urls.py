
from django.urls import path, re_path
from . import views
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import include, url

urlpatterns = [
    path('', views.index, name='index'),
    url(r'^listar/$', views.lista_tecnico, name='listar' ),
    url(r'^nuevo/$', views.crear_tecnico, name='nuevo'),
    path(r'^tecnico/actualizar/<id>/', views.actualizar_tecnico, name='actualizar_tecnico'),
    path(r'^tecnico/eliminar/<id>/', views.eliminar_tecnico, name='eliminar_tecnico'),
    url(r'^listaritem/$', views.lista_item, name='listaritem' ),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns +=static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
