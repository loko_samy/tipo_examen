from django.shortcuts import render
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm, UserChangeForm
from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import (get_object_or_404, redirect, render, render_to_response)
from .models import Tecnico
from .forms import TecnicoForm
from django.contrib import auth, messages
from django.core.mail import EmailMessage
from django.core.mail import send_mail
from django.conf import settings
from django.core import serializers
import json

#index
def index(request):
    return render(request, 'index.html', {})


#lista tecnicos
def lista_tecnico(request):
    form = Tecnico.objects.all()
    return render(request, 'listar.html', {'form': form})


#crea tecnicos
def crear_tecnico(request):
    if request.method=='POST':
        formulario = TecnicoForm(request.POST, request.FILES)
        if formulario.is_valid():
            formulario.save()
            return HttpResponseRedirect('/')
    else:
        formulario =TecnicoForm()
    context = {'formulario': formulario}
    return render(request, 'crear_tecnico.html', context)

#actualiza tecnicos
def actualizar_tecnico(request, id):
    tecnico = Tecnico.objects.get(id=id)
    form = TecnicoForm(request.POST or None, instance=tecnico)
    if form.is_valid():
        form.save()
        return render(request,'index.html')
    else:
        
     return render(request, 'actualizar_tecnico.html', {'form': form , 'tecnico': tecnico})
    

#eliminar tecnicos
def eliminar_tecnico(request, id):
    tecnico = Tecnico.objects.get(id=id)
    if request.method=='POST':
        tecnico.delete()
        return render(request,'index.html')
    else:
        return render(request, 'eliminar_tecnico.html', {'tecnico':tecnico})


def lista_item(request):
    form = Tecnico.objects.all()
    return render(request, 'listar_item.html', {'form': form})